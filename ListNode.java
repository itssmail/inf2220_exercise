public class ListNode {
    public int destination;
    public ListNode next;
    
    public ListNode(int dest, ListNode next) {
	destination = dest;
	this.next = next;
    }

    public static void main (String[] args) throws java.lang.Exception {
	ListNode[] ListNodeArray = new ListNode[6];
	ListNodeArray[0] = null;
	ListNodeArray[1] = new ListNode(2, new ListNode(3, new ListNode(4,null)));
	ListNodeArray[2] = new ListNode(4, new ListNode(5,null));
	ListNodeArray[3] = null;
	ListNodeArray[4] = new ListNode(3, null);
	ListNodeArray[5] = new ListNode(4, null);
	int[] inDeg = calculateIndegrees(ListNodeArray);
	for (int i=1; i < inDeg.length; i++) {
	    System.out.println("Indegress for node " + i + " is " + inDeg[i]);
	}
    }
    
public static int[] calculateIndegrees(ListNode[] listArray) {
    int[] inDegreesArray = new int[listArray.length];
    for (int i=1;i < listArray.length; i++) {
	ListNode node = listArray[i];
	while (node != null) {
	    inDegreesArray[node.destination]++;
	    node = node.next;
	}
    } return inDegreesArray;
}
}



			 
    
